package com.flash.light;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/15/12
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyLightGreenActivity extends Activity {

    TextView phoneDetails;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.green);

        Button redButton = (Button)findViewById(R.id.red_button);

        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setupView();
    }

    private void setupView() {
        phoneDetails = (TextView)findViewById(R.id.phone_state);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        phoneDetails.setText(telephonyManager.getDeviceId());

    }
}
